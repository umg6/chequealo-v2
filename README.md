# Chequealo V2

## Flow for User Registration and User Login
For JWT – Token based Authentication with Web API, we’re gonna call 2 endpoints:
- POST `api/v1/auth/register` for User Registration
- POST `api/v1/auth/login` for User Login

You can take a look at following flow to have an overview of Requests and Responses that Angular 12 Client will make or receive.

![angular-12-jwt-authentication-flow](angular-12-jwt-authentication-flow.png)

## Angular JWT App Diagram with Router and HttpInterceptor
![angular-12-jwt-authentication-overview](angular-12-jwt-authentication-overview.png)

// const TOKEN_HEADER_KEY = 'Authorization'; // for Spring Boot back-end
const TOKEN_HEADER_KEY = 'x-access-token';   // for Node.js Express back-end

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  ...

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    ...
    if (token != null) {
      // for Spring Boot back-end
      // authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });

      // for Node.js Express back-end
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, token) });
    }
    return next.handle(authReq);
  }
}

...
```

Run `ng serve --port 8081` for a dev server. Navigate to `http://localhost:8081/`.