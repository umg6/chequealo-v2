import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../_services/token-storage.service';

const API_URL = 'http://localhost:8080/api/v1/';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }

  getProfileContent(): Observable<any> {
    var username = "undefined";
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Credentials' : 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': this.tokenStorage.getUser().bearer,
      })
    };
    
    if (this.tokenStorage.getUser()) {
      username = this.tokenStorage.getUser().username;
    }

    return this.http.get(API_URL + 'profiles/' + username, httpOptions);
  }

  getUserBoard(): Observable<any> {
    var username = "undefined";
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Credentials' : 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': this.tokenStorage.getUser().bearer,
      })
    };
    
    if (this.tokenStorage.getUser()) {
      username = this.tokenStorage.getUser().username;
    }

    return this.http.get(API_URL + 'profiles/' + username + '/markings', httpOptions);
  }

  generateMark(): Observable<any> {
    var username = "undefined";
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Credentials' : 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': this.tokenStorage.getUser().bearer,
      })
    };

    if (this.tokenStorage.getUser()) {
      username = this.tokenStorage.getUser().username;
    }

    return this.http.post(API_URL + 'profiles/' + username + '/markings', {}, httpOptions);
  }

  updateMark(markingId: Number): Observable<any> {
    var username = "undefined";
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Credentials' : 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': this.tokenStorage.getUser().bearer,
      })
    };

    if (this.tokenStorage.getUser()) {
      username = this.tokenStorage.getUser().username;
    }

    return this.http.put(API_URL + 'profiles/' + username + '/markings/' + markingId, {}, httpOptions);
  }

  getAdminBoard(): Observable<any> {
    var username = "undefined";
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Credentials' : 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': this.tokenStorage.getUser().bearer,
      })
    };
    
    if (this.tokenStorage.getUser()) {
      username = this.tokenStorage.getUser().username;
    }

    return this.http.get(API_URL + 'admin', httpOptions);
  }
}
