import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {
  currentUser: any;
  content?: any;
  markingId?: any;
  buttonMessage?: String;
  buttonAttrs?: String;

  constructor(private token: TokenStorageService, private userService: UserService) { }

  ngOnInit(): void 
  {
    this.currentUser = this.token.getUser();
    this.buttonMessage = "Marcar entrada";
    this.buttonAttrs = '';

    this.userService.getUserBoard().subscribe(
      data => {
        this.content = data.data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }

  generateMarking(): void 
  {
    if (this.markingId) {
      this.userService.updateMark(this.markingId).subscribe(
        data => {
          this.content = data.data;
          this.buttonMessage = "Has marcado tu jornada";
          this.buttonAttrs = 'disabled';
        },
        err => {
          this.content = JSON.parse(err.error).message;
        }
      );
    } else {
      this.userService.generateMark().subscribe(
        data => {
          console.log(data);
          this.content = data.data;
          this.markingId = data.data.id;
          this.buttonMessage = "Marcar salida";
          this.buttonAttrs = '';
        },
        err => {
          this.content = JSON.parse(err.error).message;
        }
      );
    }
  }
}
